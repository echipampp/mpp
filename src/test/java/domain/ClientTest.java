package domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ClientTest {

    private static final Long id = new Long(1);
    private static final String cnp = "123";
    private static final String name = "Test";
    private static final String phone = "123456";

    private static final String new_cnp = "456";
    private static final String new_name = "Test new";
    private static final String new_phone = "1234566666";

    private Client client;

    @Before
    public void setUp() throws Exception {
        client = new Client(id, cnp, name, phone);
    }

    @After
    public void tearDown() throws Exception {
        client = null;
    }

    @Test
    public void testGetId() throws Exception {
        assertEquals("IDs should be equal", id, client.getId());
    }

    @Test
    public void testGetCnp() throws Exception {
        assertEquals("CNPs should be equal", cnp, client.getCnp());
    }

    @Test
    public void testGetName() throws Exception {
        assertEquals("Names should be equal", name, client.getName());
    }

    @Test
    public void testGetPhone() throws Exception {
        assertEquals("Phone numbers should be equal", phone, client.getPhone());
    }

    @Test
    public void testSetCnp() throws Exception {
        client.setCnp(new_cnp);
        assertEquals("CNPs should be equal", new_cnp, client.getCnp());
    }

    @Test
    public void testSetName() throws Exception {
        client.setName(new_name);
        assertEquals("Names should be equal", new_name, client.getName());
    }

    @Test
    public void testSetPhone() throws Exception {
        client.setPhone(new_phone);
        assertEquals("Phone numbers should be equal", new_phone, client.getPhone());
    }
}