package domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BuyTest {

    private static final Long id = 1L;
    private static final Long client_id = 1L;
    private static final Long book_id = 1L;

    private static final Long new_client_id = 2L;
    private static final Long new_book_id = 2L;

    private Buy buy;

    @Before
    public void setUp() throws Exception {
        buy = new Buy(client_id, book_id);
        buy.setId(id);
    }

    @After
    public void tearDown() throws Exception {
        buy = null;
    }

    @Test
    public void getClient_id() throws Exception {
        assertEquals("Values should be equal", client_id, buy.getClient_id());
    }

    @Test
    public void setClient_id() throws Exception {
        buy.setClient_id(new_client_id);
        assertEquals("Values should be equal", new_client_id, buy.getClient_id());
    }

    @Test
    public void getBook_id() throws Exception {
        assertEquals("Values should be equal", book_id, buy.getBook_id());
    }

    @Test
    public void setBook_id() throws Exception {
        buy.setBook_id(new_book_id);
        assertEquals("Values should be equal", new_book_id, buy.getBook_id());
    }

}