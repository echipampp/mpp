package domain;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class BookTest {

    private static final Long id = new Long(1);
    private static final String name = "Test1";
    private static final String author = "Author1";

    private static final String new_name = "Name new";
    private static final String new_author = "Author new";

    private Book book;

    @Before
    public void setUp() throws Exception {
        book = new Book(name, author);
        book.setId(id);
    }

    @After
    public void tearDown() throws Exception {
        book = null;
    }

    @Test
    public void getName() throws Exception {
        assertEquals("Names should be equal", name, book.getName());
    }

    @Test
    public void setName() throws Exception {
        book.setName(new_name);
        assertEquals("Names should be equal", new_name, book.getName());
    }

    @Test
    public void getAuthor() throws Exception {
        assertEquals("Authors should be equal", author, book.getAuthor());
    }

    @Test
    public void setAuthor() throws Exception {
        book.setName(new_author);
        assertEquals("Authors should be equal", new_author, book.getAuthor());

    }

}