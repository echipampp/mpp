package controller;

import domain.Book;
import domain.Buy;
import domain.Client;
import domain.validator.ValidatorException;
import repository.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class Controller {

    private Repository<Long, Client> repoClient; /**/
    private Repository<Long, Book> repoBook;
    private Repository<Long, Buy> repoBuy;

    /**
     * constructor
     * @param repository
     * @param repo
     * @param repoBuy
     */
    public Controller(Repository<Long, Client> repository,Repository<Long, Book> repo, Repository<Long, Buy> repoBuy) {
        this.repoClient = repository;
        this.repoBook= repo;
        this.repoBuy=repoBuy;
    }

    public void addClient(Client c) throws ValidatorException {
        repoClient.save(c);
    }

    public void updateClient(Client c) throws ValidatorException
    {
        repoClient.update(c);
    }

    public void deleteClient(Long id) throws ValidatorException
    {
        repoClient.delete(id);
    }

    public Optional<Client> getClient(Long id) throws ValidatorException
    {
        return repoClient.findOne(id);
    }

    public void addBook(Book m) throws ValidatorException {
        repoBook.save(m);
    }

    public void updateBook(Book m) throws ValidatorException
    {
        repoBook.update(m);
    }

    public void deleteBook(Long id) throws ValidatorException
    {
        repoBook.delete(id);
    }
    public Optional<Book> getBook(Long id) throws ValidatorException
    {
        return repoBook.findOne(id);
    }

    public void addBuy(Buy b) throws ValidatorException {
        repoBuy.save(b);
    }

    public void updateBuy(Buy b) throws ValidatorException
    {
        repoBuy.update(b);
    }

    public void deleteBuy(Long id) throws ValidatorException {
        repoBuy.delete(id);
    }

    public Optional<Buy> getBuy(Long id) throws ValidatorException
    {
        return repoBuy.findOne(id);
    }

    /**
     * the set of all clients
     * @return clients
     */
    public Set<Client> getAllClients() {
        Iterable<Client> clients = repoClient.findAll();
        return StreamSupport.stream(clients.spliterator(), false)
                .collect(Collectors.toSet());
    }

    /**
     * the set of all movies
     * @return movies
     */
    public Set<Book> getAllBooks() {
        Iterable<Book> books = repoBook.findAll();
        return StreamSupport.stream(books.spliterator(), false)
                .collect(Collectors.toSet());
    }

    /**
     * get all the borrows
     * @return borrows
     */
    public Set<Buy> getAllBuys() {
        Iterable<Buy> buys = repoBuy.findAll();
        return StreamSupport.stream(buys.spliterator(), false)
                .collect(Collectors.toSet());}


    /**
     * find the clients by a specific name
     * @param name string
     * @return set of clients
     */
    public Set<Client> filterClientByName(String name)
    {
        Iterable<Client> clients = repoClient.findAll();
        return StreamSupport.stream(clients.spliterator(), false)
                .filter(client -> client.getName().contains(name))
                .collect(Collectors.toSet());
    }

    /**
     * find the movies with a specified name
     * @param name string
     * @return set of movies
     */
    public Set<Book> filterBookByName(String name)
    {
        Iterable<Book> books = repoBook.findAll();
        return StreamSupport.stream(books.spliterator(), false)
                .filter(book -> book.getName().contains(name))
                .collect(Collectors.toSet());
    }

    /**
     * find the movie with a specified director
     * @param name string
     * @return set of movies
     */
    public Set<Book> filterBookByAuthor(String name)
    {
        Iterable<Book> books = repoBook.findAll();
        return StreamSupport.stream(books.spliterator(), false)
                .filter(book -> book.getAuthor().contains(name))
                .collect(Collectors.toSet());
    }

    /**
     * find the borrows with a specified client
     * @param id int
     * @return set of borrows
     */
    public Set<Buy> filterBuyByClient(Long id)
    {
        Iterable<Buy> buys = repoBuy.findAll();
        return StreamSupport.stream(buys.spliterator(), false)
                .filter(buy -> buy.getClient_id() == id)
                .collect(Collectors.toSet());
    }
    /**
     * find most rented movies
     */
    public Optional<Buy> mostRented()
    {
        Iterable<Buy> buys = repoBuy.findAll();
        List<Buy> b = StreamSupport.stream(buys.spliterator(),false)
                .collect(Collectors.toList());
        ArrayList<Long> li = new ArrayList<>();
        for(int j = 0; j<b.size(); j++)
        {
            int x = 0;
            for(int i = j; i<b.size(); i++)
            {
                if(b.get(i).getBook_id() == b.get(j).getBook_id()){
                    x++;
                }
            }
            li.add(Long.valueOf(x));
            li.add(b.get(j).getBook_id());
        }

        Integer max = 0;
        Integer val = 0;

        for(int i=0; i<li.size(); i+=2)
            if (max < li.get(i)) {
                max = Math.toIntExact(li.get(i));
                val = Math.toIntExact(li.get(i + 1));
            }

        return repoBuy.findOne(val.longValue());
    }
}
