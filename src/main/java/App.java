import console.Console;
import controller.Controller;
import domain.Book;
import domain.Buy;
import domain.Client;
import domain.validator.BookValidator;
import domain.validator.BuyValidator;
import domain.validator.ClientValidator;
import domain.validator.Validator;
import jdk.internal.org.xml.sax.SAXException;
import repository.*;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;

public class App {

    /*
    TODO (1) - Optional is deprecated
    TODO (2) - write general class for XMLReader/ XMLWriter
    TODO (3) - Test classes
     */

    public static void main(String[] args){

        //in file repo
        Validator<Client> clientValidator = new ClientValidator();
        Validator<Book> bookValidator = new BookValidator();
        Validator<Buy> buyValidator = new BuyValidator();

        //save in a text file
        /**
         Repository<Long, Client> clientrepo = new ClientFileRepo(clientValidator, "./data/clients");
         Repository<Long, Movie> bookrepo = new MovieFileRepo(movieValidator, "./data/movie");
         Repository<Long, Borrow> buyrepo = new BorrowFileRepo(borrowValidator, "./data/borrow");
         **/

        //save in a XML file
//        Repository<Long, Client> clientrepo = null;
        Repository<Long, Book> bookrepo = null;
        Repository<Long, Buy> buyrepo = null;
        try {
//            clientrepo = new ClientXMLRepo(clientValidator, "./data/client.xml");
//            clientrepo = new ClientFileRepo(clientValidator, "./data/client.txt");
            bookrepo = new BookXMLRepo(bookValidator, "./data/book.xml");
            buyrepo = new BuyXMLRepo(buyValidator, "./data/buy.xml");
        } catch (IOException | SAXException | ParserConfigurationException e) {
            e.printStackTrace();
        }


        // save into database
        String url = "jdbc:postgresql://localhost:5432/bookstore";
        Repository<Long, Client> clientrepo = new ClientDBRepo(clientValidator, url, "postgres", "123");


        Controller ctrl = new Controller(clientrepo,bookrepo,buyrepo);
        Console console = new Console(ctrl);
        console.runConsole();
    }

}
