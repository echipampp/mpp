package console;


import controller.Controller;
import domain.Book;
import domain.Buy;
import domain.Client;
import domain.validator.ValidatorException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.InputMismatchException;
import java.util.Optional;
import java.util.Scanner;
import java.util.Set;


public class Console {
    private Controller ctrl;

    public Console(Controller ctrl) {
        this.ctrl = ctrl;
    }

    public void menu() {
        System.out.print("\n\n------------Book Store --------\n\n" +
                "1. Client\n" +
                "2. Book\n" +
                "3. Buy book\n" +
                "4. Exit\n");
    }

    public void menuClient()
    {
        System.out.print("\n ---- Client ----\n" +
                "1. Print all clients\n" +
                "2. Add a client\n" +
                "3. Update a client\n" +
                "4. Delete a client\n" +
                "5. Find client\n" +
                "6. Filter clients by name\n" +
                "0. Main menu\n");
    }

    public void menuBuy()
    {
        System.out.print("\n ---- Rent ----\n" +
                "1. Print all buys\n" +
                "2. New buy\n" +
                "3. Update a buy\n" +
                "4. Delete a buy\n" +
                "5. Find buy\n" +
                "6. Filter buy by client\n" +
                "0. Main menu\n");
    }

    public void menuBook()
    {
        System.out.print("\n---- Book ----\n" +
                "1. Print all books\n" +
                "2. Add a book\n" +
                "3. Update a book\n" +
                "4. Delete a book\n" +
                "5. Find book\n" +
                "6. Filter books by name\n" +
                "7. Filter books by author\n" +
                "8. Most buyed book\n" +
                "0. Main menu\n");
    }

    public void runConsole() {
        while (true) {
            menu();
            int n = inputOption();
            if (n == 1) {
                while(true) {
                    menuClient();
                    int i = inputOption();
                    if (i == 1)
                        uiPrintClients();
                    if (i == 2)
                        uiAddClient();
                    if (i == 3)
                        uiUpdateClient();
                    if (i == 4)
                        uiDeleteClient();
                    if (i == 5)
                        uiFindClient();
                    if (i == 6)
                        uiFilterClient();
                    if (i == 0)
                        break;
                }
            }
            if(n == 2) {
                while (true) {
                    menuBook();
                    int i = inputOption();
                    if (i == 1)
                        uiPrintBooks();
                    if (i == 2)
                        uiAddBooks();
                    if (i == 3)
                        uiUpdateBook();
                    if (i == 4)
                        uiDeleteBook();
                    if (i == 5)
                        uiFindBook();
                    if (i == 6)
                        uiFilterBookByName();
                    if (i == 7)
                        uiFilterBookByAuthor();
                    if (i == 8)
                        report();
                    if (i == 0)
                        break;
                }
            }
            if( n==3 ) {
                while (true) {
                    menuBuy();
                    int i = inputOption();
                    if (i == 1)
                        uiPrintBuys();
                    if (i == 2)
                        uiAddBuys();
                    if (i == 3)
                        uiUpdateBuy();
                    if (i == 4)
                        uiDeleteBuy();
                    if (i == 5)
                        uiFindBuy();
                    if (i == 6)
                        uiFilterBuyByClient();
                    if (i == 0)
                        break;
                }
            }
            if(n == 4)
            {
                break;
            }
        }
    }

    public int inputOption() {
        try {
            System.out.print("Choose the option: ");
            Scanner s = new Scanner(System.in);
            int n = s.nextInt();
            System.out.print(" \n           ~           *           ~        \n");
            return n;
        } catch (InputMismatchException e) {
            System.out.println("An int requested!!");
        }
        return 0;
    }

    private void uiPrintClients() {
        Set<Client> clients = ctrl.getAllClients();
        clients.stream().forEach(System.out::println);
    }

    private void uiAddClient() {
        Client client = readClient();
        try {
            ctrl.addClient(client);
        } catch (ValidatorException e) {
            System.out.print(e.toString());
        }
    }

    private void uiUpdateClient(){
        Client c = readClient();
        try {
            ctrl.updateClient(c);
        } catch (ValidatorException e) {
            System.out.print(e.toString());
        }
    }

    private void uiDeleteClient() {
        System.out.println("Give the ID of the client you want to delete:");
        Long id;
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            id = Long.valueOf(bufferRead.readLine());// ...
            ctrl.deleteClient(id);
        } catch (IOException | ValidatorException ex) {
            System.out.print(ex.toString());
        }
    }

    private void uiFindClient()  {
        System.out.println("Give the ID of the client you are locking for:");
        Long id;
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            id = Long.valueOf(bufferRead.readLine());// ...
            Optional<Client> client = ctrl.getClient(id);
            System.out.println(client);
        } catch (IOException | ValidatorException ex) {
            System.out.print(ex.toString());
        }
    }

    private void uiFilterClient()
    {
        System.out.println("Give the name of the client you are locking for:");
        String name;
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            name = bufferRead.readLine();
            ctrl.filterClientByName(name);

            Set<Client> clients = ctrl.filterClientByName(name);
            clients.stream().forEach(System.out::println);
        } catch (IOException ex) {
            System.out.print(ex.toString());
        }
    }

    private Client readClient() {
        System.out.println("Id, CNP, Name and Phone are required:\n");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            Long id = Long.valueOf(bufferRead.readLine());// ...
            String cnp =  bufferRead.readLine();
            String name = bufferRead.readLine();
            String phone = bufferRead.readLine();// ...

            Client client = new Client(id, cnp, name, phone);
            client.setId(id);

            return client;
        } catch (IOException ex) {
            System.out.print(ex.toString());
        }
        return null;
    }

    private void uiPrintBooks() {
        Set<Book> books = ctrl.getAllBooks();
        books.stream().forEach(System.out::println);
    }

    private void uiAddBooks() {
        Book book = readBook();
        try {
            ctrl.addBook(book);
        } catch (ValidatorException e) {
            System.out.print(e.toString());
        }
    }

    private void uiUpdateBook(){
        Book m = readBook();
        try {
            ctrl.updateBook(m);
        } catch (ValidatorException e) {
            System.out.print(e.toString());
        }
    }

    private void uiDeleteBook(){
        System.out.println("Give the ID of the book you  want to delete:");
        Long id;
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            id = Long.valueOf(bufferRead.readLine());// ...
            ctrl.deleteBook(id);
        } catch (IOException | ValidatorException ex) {
            System.out.print(ex.toString());
        }
    }

    private void uiFindBook() {
        System.out.println("Give the ID of the book you are locking for:");
        Long id;
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            id = Long.valueOf(bufferRead.readLine());// ...
            ctrl.getBook(id);
            Optional<Buy> b = ctrl.getBuy(id);

            System.out.println(b);
        } catch (IOException | ValidatorException ex) {
            System.out.print(ex.toString());
        }
    }

    private void uiFilterBookByName()
    {
        System.out.println("Give the name of the book you are locking for:");
        String name;
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            name = bufferRead.readLine();

            Set<Book> books = ctrl.filterBookByName(name);
            books.stream().forEach(System.out::println);
        } catch (IOException ex) {
            System.out.print(ex.toString());
        }
    }

    private void uiFilterBookByAuthor()
    {
        System.out.println("Give the name of the author you are locking for:");
        String name;
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            name = bufferRead.readLine();

            Set<Book> books = ctrl.filterBookByAuthor(name);
            books.stream().forEach(System.out::println);
        } catch (IOException ex) {
            System.out.print(ex.toString());
        }

    }

    private Book readBook() {
        System.out.println("Id, Name and Author of the book are required:\n");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            Long id = Long.valueOf(bufferRead.readLine());// ...
            String name = bufferRead.readLine();
            String author = bufferRead.readLine();// ...

            Book book = new Book(name, author);
            book.setId(id);

            return book;
        } catch (IOException ex) {
            System.out.print(ex.toString());
        }
        return null;
    }
    private void uiPrintBuys() {
        Set<Buy> books = ctrl.getAllBuys();
        books.stream().forEach(System.out::println);
    }

    private void uiAddBuys() {
        Buy buy = readBuy();
        try {
            ctrl.addBuy(buy);
        } catch (ValidatorException e) {
            System.out.print(e.toString());
        }
    }

    private void uiUpdateBuy() {
        Buy b = readBuy();
        try {
            ctrl.updateBuy(b);
        } catch (ValidatorException e) {
            System.out.print(e.toString());
        }
    }

    private void uiDeleteBuy() {
        System.out.println("Give the ID of the buy you want to delete:");
        Long id;
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            id = Long.valueOf(bufferRead.readLine());// ...
            ctrl.deleteBuy(id);
        } catch (IOException | ValidatorException ex) {
            System.out.print(ex.toString());
        }
    }

    private void uiFindBuy() {
        System.out.println("Give the ID of the buy you are locking for:");
        Long id;
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            id = Long.valueOf(bufferRead.readLine());// ...
            Optional<Buy> b = ctrl.getBuy(id);

            System.out.println(b.toString());
        } catch (IOException | ValidatorException ex) {
            System.out.print(ex.toString());
        }
    }

    private void uiFilterBuyByClient()
    {
        System.out.println("Give the ID of the borrow you are locking for:");
        Long id;
        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            id = Long.valueOf(bufferRead.readLine());// ...
            Set<Buy> books = ctrl.filterBuyByClient(id);

            books.stream().forEach(System.out::println);
        } catch (IOException ex) {
            System.out.print(ex.toString());
        }
    }

    private Buy readBuy() {
        System.out.println("Id of the buy, client and the book required:\n");

        BufferedReader bufferRead = new BufferedReader(new InputStreamReader(System.in));
        try {
            Long id = Long.valueOf(bufferRead.readLine());// ...
            Long idClient =  Long.valueOf(bufferRead.readLine());
            Long idBook =  Long.valueOf(bufferRead.readLine());

            Buy buy = new Buy(idClient, idBook);
            buy.setId(id);

            return buy;
        } catch (IOException ex) {
            System.out.print(ex.toString());
        }
        return null;
    }

    private void report()
    {
        Optional<Buy> b =  ctrl.mostRented();

        System.out.println(b);
    }
}
