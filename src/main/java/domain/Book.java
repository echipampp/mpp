package domain;

/**
 * Created by iulia on 15.03.2017.
 */
public class Book extends BaseEntity<Long> {

    private String name;
    private String author;

    /**
     * Constructor of the entity
     *
     * @param name   string
     * @param author string
     */
    public Book(String name, String author) {
        this.name = name;
        this.author = author;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }


    public String toString()
    {
        return "Book ~ Name: " + this.name + ", " +
                "Author: " + this.author;
    }
}
