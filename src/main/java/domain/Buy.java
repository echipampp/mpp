package domain;

public class Buy extends BaseEntity<Long> {

    private Long client_id;
    private Long book_id;

    /**
     * Constructor of the entity
     * @param client int
     * @param book int
     */
    public Buy( Long client, Long book)
    {
        this.client_id = client;
        this.book_id = book;
    }

    public Long getClient_id() {
        return client_id;
    }

    public void setClient_id(Long client_id) {
        this.client_id = client_id;
    }

    public Long getBook_id() {
        return book_id;
    }

    public void setBook_id(Long book_id) {
        this.book_id = book_id;
    }

    public String toString()
    {
        return "Client id: " + client_id + "\n"+
                "Book id: " + book_id + "\n";
    }
}
