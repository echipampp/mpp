package domain;

/**
 * Created by iulia on 15.03.2017.
 */
public class BaseEntity<ID> {
    private ID id;

    public ID getId() {
        return id;
    }

    public void setId(ID id) {
        this.id = id;
    }

    /**
     * the id of the entity
     * @return string
     */
    @Override
    public String toString() {
        return ", id=" + id;
    }
}
