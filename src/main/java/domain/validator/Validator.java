package domain.validator;

public interface Validator<T> {
    /**
     * validate the input
     * @param type t
     * @throws ValidatorException
     */
    void validate(T type) throws ValidatorException;
}
