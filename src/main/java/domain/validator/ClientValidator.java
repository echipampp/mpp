package domain.validator;

import domain.Client;

public class ClientValidator implements Validator<Client>{

    public void validate(Client client) throws ValidatorException
    {
        String error = "";
        if (client.getCnp().equals(""))
        {
            error += "Client CNP is null\n";
        }

        if (client.getName().equals(""))
        {
            error += "Client name is null\n";
        }
        if (client.getPhone().equals(""))
        {
            error += "Client phone is null\n";
        }

        if (!error.equals(""))
        {
            throw new ValidatorException(error);
        }
    }

}
