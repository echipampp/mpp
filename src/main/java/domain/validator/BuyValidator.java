package domain.validator;

import domain.Buy;

public class BuyValidator implements Validator<Buy> {

    public void validate(Buy buy) throws ValidatorException
    {
        String error = "";

        if (buy.getClient_id() == 0)
        {
            error += "Client ID is null\n";
        }
        if (buy.getBook_id() == 0)
        {
            error += "Book ID is null\n";
        }

        if (!error.equals(""))
        {
            throw new ValidatorException(error);
        }
    }
}
