package domain.validator;

import domain.Book;

public class BookValidator implements Validator<Book> {

    public void validate(Book book) throws ValidatorException
    {
        String error = "";
        if (book.getName().equals(""))
        {
            error += "Book name is null\n";
        }
        if (book.getAuthor().equals(""))
        {
            error += "Book author is null\n";
        }

        if (!error.equals(""))
        {
            throw new ValidatorException(error);
        }
    }
}
