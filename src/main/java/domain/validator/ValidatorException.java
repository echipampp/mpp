package domain.validator;

/**
 * Created by iulia on 15.03.2017.
 */
public class ValidatorException extends Exception {

    public ValidatorException(String msg)
    {
        super(msg);
    }
}
