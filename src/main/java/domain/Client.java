package domain;

/**
 * Created by iulia on 15.03.2017.
 */
public class Client extends BaseEntity<Long> {

    private Long id;
    private String cnp;
    private String name;
    private String phone;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * Constructor of the entity
     * @param cnp String
     * @param name string
     * @param phone string
     */
    public Client(Long id, String cnp, String name, String phone)
    {
        this.id = id;
        this.cnp=cnp;

        this.name = name;
        this.phone = phone;
    }

    public String getCnp()
    {
        return cnp;
    }

    public void setCnp(String cnp)
    {
        this.cnp=cnp;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getPhone()
    {
        return phone;
    }

    public void setPhone(String phone)
    {
        this.phone = phone;
    }

    @Override
    public String toString()
    {
        return "Client ~ CNP : " + cnp +", "+
                "Name : " + name + ", "+
                "Phone number : " + phone + super.toString();
    }
}
