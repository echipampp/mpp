package repository;

import domain.Book;
import domain.validator.Validator;
import domain.validator.ValidatorException;
import jdk.internal.org.xml.sax.SAXException;
import util.xml.XMLBookReader;
import util.xml.XMLBookWriter;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

public class BookXMLRepo extends InMemoryRepository<Long, Book> {

    private String fileName;

    public BookXMLRepo(Validator<Book> validator, String fileName) throws IOException, SAXException, ParserConfigurationException {
        super(validator);
        this.fileName = fileName;
        loadData();
    }

    public void loadData() throws ParserConfigurationException, IOException, SAXException
    {
        List<Book> books = null;
        try {
            books = new XMLBookReader<Long, Book>(fileName).load();
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        } catch (org.xml.sax.SAXException e) {
            e.printStackTrace();
        }

        for (Book book: books)
            try {
                super.save(book);
            } catch (ValidatorException e) {
                e.printStackTrace();
            }
    }

    @Override
    public Optional<Book> save(Book book) throws ValidatorException{
        Optional<Book> optional = super.save(book);
        if(optional.isPresent())
            return optional;
        try {
            new XMLBookWriter<Long, Book>(fileName).save(book);
        } catch (ParserConfigurationException | IOException | TransformerException e) {
            e.printStackTrace();
        } catch (org.xml.sax.SAXException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<Book> delete(Long id)
    {
        return super.delete(id);
    }

    @Override
    public Optional<Book> update(Book book) throws ValidatorException {
        return super.update(book);
    }
}
