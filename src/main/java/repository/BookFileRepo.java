package repository;

import domain.Book;
import domain.validator.Validator;
import domain.validator.ValidatorException;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class BookFileRepo extends InMemoryRepository<Long, Book> {
        private String fileName;

        public BookFileRepo(Validator<Book> validator, String fileName) {
            super(validator);
            this.fileName = fileName;

            loadData();
        }

        private void loadData() {
            Path path = Paths.get(fileName);

            try {
                Files.lines(path).forEach(line -> {
                    List<String> items = Arrays.asList(line.split(","));

                    Long id = Long.valueOf(items.get(0));
                    String name = items.get(1);
                    String author = items.get((2));

                    Book book = new Book( name, author);
                    book.setId(id);

                    try {
                        super.save(book);
                    } catch (ValidatorException e) {
                        e.printStackTrace();
                    }
                });
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        @Override
        public Optional<Book> save(Book entity) throws ValidatorException {
            Optional<Book> optional = super.save(entity);
            if (optional.isPresent()) {
                return optional;
            }
            saveToFile(entity);
            return Optional.empty();
        }

        @Override
        public Optional<Book> delete(Long aLong) {
            return super.delete(aLong);
        }

        @Override
        public Optional<Book> update(Book entity) throws ValidatorException {
            return super.update(entity);
        }

        private void saveToFile(Book entity) {
            Path path = Paths.get(fileName);

            try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
                bufferedWriter.write(
                        entity.getId() + "," + entity.getName() + "," + entity.getAuthor());
                bufferedWriter.newLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
}
