package repository;

import domain.Client;
import domain.validator.Validator;
import domain.validator.ValidatorException;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ClientDBRepo implements Repository<Long, Client>{

    private Validator<Client> validator;
    private String url;
    private String username;
    private String password;

    public ClientDBRepo(Validator<Client> clientValidator, String url, String username, String password) {
        this.validator = clientValidator;
        this.url = url;
        this.username = username;
        this.password = password;
    }



    @Override
    public Optional<Client> findOne(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("id must not be null");
        }
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("select * from client where id=?")) {
            statement.setLong(1, id);
            try (ResultSet resultSet = statement.executeQuery()) {
                if (resultSet.next()) {
                    Long clientId = resultSet.getLong("id");
                    String cnp = resultSet.getString("cnp");
                    String name = resultSet.getString("name");
                    String phoneNumber = resultSet.getString("phone");

                    Client student = new Client(clientId, cnp, name, phoneNumber);
                    student.setId(clientId);
                    return Optional.of(student);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Iterable<Client> findAll() {
        List<Client> clients = new ArrayList<>();
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement("select * from client")) {
            try (ResultSet resultSet = statement.executeQuery()) {
                while (resultSet.next()) {
                    Long clientId = resultSet.getLong("id");
                    String cnp = resultSet.getString("cnp");
                    String name = resultSet.getString("name");
                    String phoneNumber = resultSet.getString("phone");

                    Client client = new Client(clientId, cnp, name, phoneNumber);
                    clients.add(client);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return clients;
    }

    @Override
    public Optional<Client> save(Client entity) throws ValidatorException {
        if (entity == null) {
            throw new IllegalArgumentException("entity must not be null");
        }
        validator.validate(entity);
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(
                     "insert into client (id, cnp, name, phone) values (?,?,?,?)")) {
            statement.setLong(1, entity.getId());
            statement.setString(2, entity.getCnp());
            statement.setString(3, entity.getName());
            statement.setString(4, entity.getPhone());

            statement.executeUpdate();

            return Optional.empty();
        } catch (SQLException e) {
            e.printStackTrace();//TODO: log exception
            return Optional.of(entity);
        }
    }

    @Override
    public Optional<Client> delete(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("cnp must not be null");
        }
        Optional<Client> client = findOne(id); // :(((
        if (!client.isPresent()) {
            return Optional.empty();
        }
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(
                     "DELETE FROM client WHERE id=?")) {
            statement.setLong(1, id);

            statement.executeUpdate();

            return client;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public Optional<Client> update(Client entity) throws ValidatorException {
        if (entity == null) {
            throw new IllegalArgumentException("entity must not be null");
        }
        validator.validate(entity);
        try (Connection connection = DriverManager.getConnection(url, username, password);
             PreparedStatement statement = connection.prepareStatement(
                     "update client set (cnp,name,phone) = (?,?,?) WHERE id=?")) {
            statement.setString(1, entity.getCnp());
            statement.setString(2, entity.getName());
            statement.setString(3, entity.getPhone());
            statement.setLong(4, entity.getId());

            statement.executeUpdate();

            return Optional.empty();
        } catch (SQLException e) {
            e.printStackTrace();
            return Optional.of(entity);
        }
    }
}
