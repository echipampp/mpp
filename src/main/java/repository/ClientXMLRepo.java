package repository;

import domain.Client;
import domain.validator.Validator;
import domain.validator.ValidatorException;
import jdk.internal.org.xml.sax.SAXException;
import util.xml.XMLClientReader;
import util.xml.XMLClientWriter;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

public class ClientXMLRepo extends InMemoryRepository<Long, Client> {
    private String file;

    public ClientXMLRepo(Validator<Client> validator, String file) throws IOException, SAXException, ParserConfigurationException {
        super(validator);
        this.file = file;
        loadData();
    }

    public void loadData() throws ParserConfigurationException, IOException, SAXException
    {
        List<Client> clients = null;
        try {
            clients = new XMLClientReader<Long, Client>(file).load();
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        } catch (org.xml.sax.SAXException e) {
            e.printStackTrace();
        }

        for (Client client: clients)
            try {
                super.save(client);
            } catch (ValidatorException e) {
                e.printStackTrace();
            }
    }

    @Override
    public Optional<Client> save(Client client) throws ValidatorException{
        Optional<Client> optional = super.save(client);
        if(optional.isPresent())
            return optional;
        try {
            new XMLClientWriter<Long, Client>(file).save(client);
        } catch (ParserConfigurationException | IOException | TransformerException e) {
            e.printStackTrace();
        } catch (org.xml.sax.SAXException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<Client> delete(Long id)
    {
        return super.delete(id);
    }

    @Override
    public Optional<Client> update(Client client) throws ValidatorException {
        return super.update(client);
    }
}