package repository;

import domain.Buy;
import domain.validator.Validator;
import domain.validator.ValidatorException;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class BuyFileRepo extends InMemoryRepository<Long, Buy> {
    private String fileName;

    public BuyFileRepo(Validator<Buy> validator, String fileName) {
        super(validator);
        this.fileName = fileName;
        loadData();
    }

    private void loadData() {
        Path path = Paths.get(fileName);

        try {
            Files.lines(path).forEach(line -> {
                List<String> items = Arrays.asList(line.split(","));

                Long idBuy = Long.valueOf(items.get(0));
                Long idClient = Long.valueOf(items.get(1));
                Long idBook = Long.valueOf(items.get(2));

                Buy b = new Buy(idClient,idBook);
                b.setId(idBuy);

                try {
                    super.save(b);
                } catch (ValidatorException e) {
                    System.out.print(e.toString());
                }
            });
        } catch (IOException ex) {
            System.out.print(ex.toString());
        }
    }

    @Override
    public Optional<Buy> save(Buy entity) throws ValidatorException {
        Optional<Buy> optional = super.save(entity);
        if (optional.isPresent()) {
            return optional;
        }
        saveToFile(entity);
        return Optional.empty();
    }

    @Override
    public Optional<Buy> delete(Long aLong) {
        return super.delete(aLong);
    }

    @Override
    public Optional<Buy> update(Buy entity) throws ValidatorException {
        return super.update(entity);
    }

    private void saveToFile(Buy entity) {
        Path path = Paths.get(fileName);

        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(path, StandardOpenOption.APPEND)) {
            bufferedWriter.write(
                    entity.getId() + "," + entity.getClient_id() + "," + entity.getBook_id());
            bufferedWriter.newLine();
        } catch (IOException e) {
            System.out.print(e.toString());
        }
    }
}
