package repository;

import domain.Buy;
import domain.Client;
import domain.validator.Validator;
import domain.validator.ValidatorException;
import jdk.internal.org.xml.sax.SAXException;
import util.xml.XMLBuyReader;
import util.xml.XMLBuyWriter;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Optional;

public class BuyXMLRepo extends InMemoryRepository<Long, Buy> {

    private String fileName;

    public BuyXMLRepo(Validator<Buy> validator, String fileName) throws IOException, SAXException, ParserConfigurationException {
        super(validator);
        this.fileName = fileName;
        loadData();
    }

    public void loadData() throws ParserConfigurationException, IOException, SAXException
    {
        List<Buy> buys = null;
        try {
            buys = new XMLBuyReader<Long, Client>(fileName).load();
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
            e.printStackTrace();
        } catch (org.xml.sax.SAXException e) {
            e.printStackTrace();
        }

        for (Buy buy: buys)
            try {
                super.save(buy);
            } catch (ValidatorException e) {
                e.printStackTrace();
            }
    }

    @Override
    public Optional<Buy> save(Buy buy) throws ValidatorException{
        Optional<Buy> optional = super.save(buy);
        if(optional.isPresent())
            return optional;
        try {
            new XMLBuyWriter<Long, Buy>(fileName).save(buy);
        } catch (ParserConfigurationException | IOException | TransformerException e) {
            e.printStackTrace();
        } catch (org.xml.sax.SAXException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public Optional<Buy> delete(Long id)
    {
        return super.delete(id);
    }

    @Override
    public Optional<Buy> update(Buy buy) throws ValidatorException {
        return super.update(buy);
    }
}
