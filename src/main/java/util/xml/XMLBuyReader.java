package util.xml;

import domain.BaseEntity;
import domain.Buy;

import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;


public class XMLBuyReader<ID, T extends BaseEntity<ID>> {

    private String fileName;

    public XMLBuyReader(String fileName)
    {
        this.fileName = fileName;
    }

    public List<Buy> load() throws ParserConfigurationException, IOException, SAXException, IllegalAccessException, InstantiationException, InvocationTargetException {
        List<Buy> clients = new ArrayList<>();

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        Document document = builder.parse(fileName);

        Node root = document.getDocumentElement();
        NodeList nodes = root.getChildNodes();

        for (int i = 0; i < nodes.getLength(); i++) {
            Node n = nodes.item(i);
            if (n instanceof Element) {
                Element el = (Element) n;
                Buy buy = createBuy(el);
                clients.add(buy);
            }
        }
        return clients;
    }

    public Buy createBuy(Element element) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        Attr attr = element.getAttributeNode("id");
        Long id = Long.parseLong(attr.getValue());

        String client = getNodeValue(element, "client");
        String book = getNodeValue(element, "buy");

        Long cl = (long) Integer.parseInt(client);
        Long mv = (long) Integer.parseInt(book);
        Buy t = new Buy(cl, mv);
        t.setId(id);
        return t;
    }

    public String getNodeValue(Element element, String nodename)
    {
        NodeList list = element.getElementsByTagName(nodename);
        Node node = list.item(0);
        return node.getTextContent();
    }
}
