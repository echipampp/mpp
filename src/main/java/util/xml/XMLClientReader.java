package util.xml;

import domain.BaseEntity;
import domain.Client;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class XMLClientReader<ID, T extends BaseEntity<ID>>
{
    private String fileName;

    public XMLClientReader(String fileName)
    {
        this.fileName = fileName;
    }

    public List<Client> load() throws ParserConfigurationException, IOException, SAXException, IllegalAccessException, InstantiationException, InvocationTargetException {
        List<Client> clients = new ArrayList<>();

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        Document document = builder.parse(fileName);

        Node root = document.getDocumentElement();
        NodeList nodes = root.getChildNodes();

        for (int i = 0; i < nodes.getLength(); i++) {
            Node n = nodes.item(i);
            if (n instanceof Element) {
                Element el = (Element) n;
                Client client = createClient(el);
                clients.add(client);
            }
        }
        return clients;
    }

    public Client createClient(Element element) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        Attr attr = element.getAttributeNode("id");
        Long id = Long.parseLong(attr.getValue());

        String cnp = getNodeValue(element, "cnp");
        String name = getNodeValue(element, "name");
        String phone = getNodeValue(element, "phone");

        Client t = new Client(id, cnp, name, phone);
        t.setId(id);
        return t;
    }

    public String getNodeValue(Element element, String nodename)
    {
        NodeList list = element.getElementsByTagName(nodename);
        Node node = list.item(0);
        return node.getTextContent();
    }
}
