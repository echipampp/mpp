package util.xml;

import domain.BaseEntity;
import domain.Book;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;


public class XMLBookReader<ID, T extends BaseEntity<ID>>  {

    private String fileName;

    public XMLBookReader(String fileName)
    {
        this.fileName = fileName;
    }

    public List<Book> load() throws ParserConfigurationException, IOException, SAXException, IllegalAccessException, InstantiationException, InvocationTargetException {
        List<Book> movies = new ArrayList<>();

        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        Document document = builder.parse(fileName);

        Node root = document.getDocumentElement();
        NodeList nodes = root.getChildNodes();

        for (int i = 0; i < nodes.getLength(); i++) {
            Node n = nodes.item(i);
            if (n instanceof Element) {
                Element el = (Element) n;
                Book book = createBook(el);
                movies.add(book);
            }
        }
        return movies;
    }

    public Book createBook(Element element) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        Attr attr = element.getAttributeNode("id");
        Long id = Long.parseLong(attr.getValue());

        String movie = getNodeValue(element, "name");
        String author = getNodeValue(element, "author");

        Book t = new Book(movie, author);
        t.setId(id);
        return t;
    }

    public String getNodeValue(Element element, String nodename)
    {
        NodeList list = element.getElementsByTagName(nodename);
        Node node = list.item(0);
        return node.getTextContent();
    }

}
