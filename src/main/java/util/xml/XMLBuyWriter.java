package util.xml;

import domain.BaseEntity;

import domain.Buy;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;


public class XMLBuyWriter<ID, T extends BaseEntity<ID>> {
    private String filename;

    public XMLBuyWriter(String fileName)
    {
        this.filename = fileName;
    }

    public void save(Buy buy) throws ParserConfigurationException, IOException, SAXException, TransformerException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = builderFactory.newDocumentBuilder();
        Document document = builder.parse(filename);

        Node root = document.getDocumentElement();
        Node node = document.createElement("buy");

        root.appendChild(node);
        Attr id = document.createAttribute("id");
        id.setValue(buy.getId().toString());
        ((Element) node).setAttributeNode(id);

        appendChildEl(document, node, "client", String.valueOf(buy.getClient_id()));
        appendChildEl(document, node, "book", String.valueOf(buy.getBook_id()));

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        DOMSource domsource = new DOMSource(document);
        Result result = new StreamResult(new File(filename));
        transformer.transform(domsource, result);
    }

    public void appendChildEl(Document document, Node node, String name, String text)
    {
        Node title = document.createElement(name);
        title.setTextContent(text);
        node.appendChild(title);
    }

}
